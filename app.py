from flask import Flask
from scripts.service.json_to_mongo_service import insertion


app = Flask(__name__)
app.register_blueprint(insertion)


if __name__ == '__main__':
    app.run()
