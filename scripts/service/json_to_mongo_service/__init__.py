from flask import Blueprint, request

from scripts.handler.json_to_mongo_handler import mongo_insertion

insertion = Blueprint('insertion_blueprint', __name__)


@insertion.route('/', methods=['POST'])
def post_data():
    content = request.get_json()
    resp = mongo_insertion(content)
    return resp
