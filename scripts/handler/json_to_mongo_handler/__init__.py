import pymongo


def mongo_insertion(data):
    client = pymongo.MongoClient('localhost', 27017)
    db = client["meghana"]
    collection = db["user_details"]
    record_created = collection.insert_many(data)
    if record_created:
        return 'Record Created'
